from brian2 import *
from inputs import *
import os
import time
import numpy

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"


# BrianLogger.suppress_hierarchy('brian2.codegen')
# BrianLogger.suppress_name('resolution_conflict')
# BrianLogger.suppress_hierarchy('brian2')


# passive
Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm
g_l = 1e-4 * siemens * cm ** -2
E_l = -70 * mV

# hh
E_Na = 58 * mV
E_K = -80 * mV
g_Na = 12 * msiemens / cm ** 2
g_K = 7 * msiemens / cm ** 2
v_th = - 63 * mV

# synaptic
E_ampa = 0 * mV
E_gaba = -75 * mV

tau_ampa = 5 * ms
tau_gaba = 5 * ms

dg_ampa = 1 * nS
dg_gaba = 1 * nS

### HERE PARAMETERS TO CHANGE ###

t_sim = 20 * ms
wgh = 0.5 # 0.5


# morphology
morphology = 'ball+stick'  # point, ball+stick
dend_shape = 'cone'  # cylinder or cone

### NUMERICAL
method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'
dt = 0.05 * ms  # 0.005

### MORPHOLOGY

diam_soma = 40 * um  # 40

# length of dendrite
length = 1000 * um # 1000 !!!
len_comp = 5 * um  # 5 !!!
n_cmp = int(length / um / (len_comp / um))

# cylinder
diam_cln = 1 * um

# cone
diam_0 = 5 # 5
diam_f = 0.5 # 0.1
beta = 0.05  # smaller more steep decrease of diameter 0.05

lengthT = linspace(0, length / um, num=n_cmp + 1)


if morphology == 'ball+stick':

    morpho = Soma(diameter=diam_soma)

    if dend_shape == 'cylinder':
        morpho.dendrite = Cylinder(diameter=diam_cln, length=length, n=n_cmp)

    if dend_shape == 'cone':
        alpha = (diam_0 - diam_f) / ((length / um) ** beta)

        diameterT = - alpha * lengthT ** beta + diam_0

        morpho.dendrite = Section(diameter=diameterT * um, length=[len_comp / um] * n_cmp * um, n=n_cmp)



date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

if not os.path.exists(
        '../DATA/HH/' + str(date_time) + '_EPSPs_test_[' + morphology + ']_[dshape_' + dend_shape + ']'):
    os.makedirs(
        '../DATA/HH/' + str(date_time) + '_EPSPs_test_[' + morphology + ']_[dshape_' + dend_shape + ']')

main_path = os.getcwd()
os.chdir('../DATA/HH/' + str(date_time) + '_EPSPs_test_[' + morphology + ']_[dshape_' + dend_shape + ']')

saved_param = {'date_time': str(date_time),
               'morphology': morphology,
               'diam_soma': diam_soma,
               'diam_0': diam_0,
               'diam_f': diam_f,
               'beta': beta,
               'length': length,
               'number_of_comp': n_cmp,
               'wgh': wgh}

np.save('parameters.npy', saved_param)

t0 = time.time()



weight_ampa = wgh
weight_gaba = wgh


# parameters from Pare 1998

eqs = """

Im = g_l * (E_l-v) + g_Na * m**3 * h * (E_Na-v) + g_K * n**4 * (E_K-v) : amp/meter**2

dm/dt = alpham * (1-m) - betam * m : 1
dh/dt = alphah * (1-h) - betah * h : 1
dn/dt = alphan * (1-n) - betan * n : 1


alpham = - 0.32/mV * (v - v_th - 13 * mV) / (exp(-(v - v_th - 13 * mV)/(4 * mV)) - 1) /ms : Hz
betam = 0.28/mV * (v - v_th - 40 * mV) / (exp((v - v_th - 40 * mV)/(5 * mV)) -1) /ms : Hz

alphah = 0.128 * exp(-(v - v_th - 17 * mV) / (18 * mV)) /ms : Hz
betah = 4 / (1 + exp(-(v - v_th - 40 * mV) / (5 * mV))) /ms : Hz

alphan = -0.032 / mV * (v - v_th - 15 * mV) / (exp(-(v - v_th - 15 * mV) / (5 * mV)) - 1) / ms : Hz
betan = 0.5 * exp(-(v - v_th - 10 * mV) / (40 * mV)) /ms : Hz

Is = g_ampa * (E_ampa - v) + g_gaba * (E_gaba - v) + I : amp (point current)

dg_ampa/dt = -g_ampa/tau_ampa : siemens
dg_gaba/dt = -g_gaba/tau_gaba : siemens

I : amp

"""


comp_of_syn_T = np.arange(0,n_cmp,5)

distanceT = []

max_depolarization_soma_T = []

max_depolarization_local_T = []

for comp_of_syn in comp_of_syn_T:

    neuron = SpatialNeuron(morphology=morpho, model=eqs, method=method, dt=dt, Cm=Cm, Ri=Ri)

    neuron.v = -70 * mV
    neuron.h = 0.
    neuron.m = 0.
    neuron.n = 0.
    neuron.I = 0.

    trace = StateMonitor(neuron, 'v', record=True)

    indices = array([0])
    times = array([0.05]) * ms  # 5 ms
    compartments = array([comp_of_syn])

    input = SpikeGeneratorGroup(len(times), indices, times)
    syn_exc = Synapses(input, neuron, on_pre='g_ampa += ' + str(weight_ampa) + '* dg_ampa')
    syn_exc.connect(i=indices.astype(int), j=compartments.astype(int))

    run(t_sim, report='text')

    distanceT = np.append(distanceT, comp_of_syn * len_comp * 10 ** 6)
    max_depolarization_soma_T = np.append(max_depolarization_soma_T, amax(trace.v[0].T) - E_l)
    max_depolarization_local_T = np.append(max_depolarization_local_T, amax(trace.v[comp_of_syn].T) - E_l)




# diameter

figure(figsize=(12, 10))

plot(lengthT, diameterT)
xlabel('distance from soma [$\mu$m]')
ylabel('diameter [$\mu$m]')

savefig('dendrite_diameter.png', dpi = 300)



# voltage at soma

figure(figsize=(12, 10))

plot(trace.t / ms, trace.v[0].T / mV)
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_soma.png', dpi = 300)


# propagation: heat map

figure(figsize=(12, 10))

contf = contourf(cumsum(neuron.length) / um, trace.t / ms, trace.v.T / mV, cmap='YlOrRd',
                 alpha=1., levels=np.linspace(np.amin(trace.v / mV), np.amax(trace.v / mV), num=100))
colorbar(contf)
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)


# plots

import matplotlib.cm as cm

c = cm.YlOrRd(0.8, 1)

figure(figsize=(12,7))


plot(distanceT, max_depolarization_soma_T * 1000, color = c)

xlabel('distance [$\mu$m]')
ylabel('depolarization at soma [mV]')
title('synaptic weight: ' + str(wgh) + ' nS')

savefig('EPSPs at soma.png')
savefig('EPSPs at soma.svg')


figure(figsize=(12,7))

plot(distanceT, max_depolarization_local_T * 1000, color = c)

xlabel('distance [$\mu$m]')
ylabel('local depolarization [mV]')
title('synaptic weight: ' + str(wgh) + ' nS')

savefig('local EPSPs.png')
savefig('local EPSPs.svg')

np.save('distance.npy', distanceT)
np.save('EPSPS at soma HH.npy', max_depolarization_soma_T * 1000)
np.save('local depolarization HH.npy', max_depolarization_local_T * 1000)





figure(figsize=(12,7))

plot(lengthT, diameterT,  color = c)

xlabel('distance [$\mu$m]')
ylabel('diameter [$\mu$m]')

savefig('diameter.png')
savefig('diameter.svg')


os.chdir(main_path)
