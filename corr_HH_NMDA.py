
import time
import os
import numpy

from brian2 import *
from inputs import *


plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"


# BrianLogger.suppress_hierarchy('brian2.codegen')
# BrianLogger.suppress_name('resolution_conflict')
# BrianLogger.suppress_hierarchy('brian2')


# ::: numerical parameters of simulation :::

t_sim = 1000 * ms
dt = 0.05 * ms  # 0.005

method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'

# ::: defining morphology :::

morphology = 'ball+stick'  # point, ball+stick
dend_shape = 'cone'  # cylinder or cone

# soma dimaeter
diam_soma = 40 * um  # 40

# length of dendrite
length = 1000 * um # 1000 !!!
len_comp = 5 * um  # 5 !!!
n_cmp = int(length / um / (len_comp / um))

# cylinder
diam_cln = 1 * um

# cone
diam_0 = 5
diam_f = 0.5 # 0.25 in AdEx, 0.5 on default here
beta = 0.05  # smaller more steep decrease of diameter 0.1

lengthT = linspace(0, length / um, num=n_cmp + 1)

# ::: construction of morphology :::

if morphology == 'point':
	length = diam_soma
	n_cmp = 1
	morpho = Soma(diameter = diam_soma)


if morphology == 'ball+stick':
	morpho = Soma(diameter=diam_soma)

	if dend_shape == 'cylinder':
		morpho.dendrite = Cylinder(diameter=diam_cln, length=length, n=n_cmp)

	if dend_shape == 'cone':

		alpha = (diam_0 - diam_f) / ((length / um) ** beta)
		diameterT = - alpha * lengthT ** beta + diam_0
		morpho.dendrite = Section(diameter=diameterT * um, length=[len_comp / um] * n_cmp * um, n=n_cmp)

# ::: differential eqs. :::   ::: parameters from Pare 1998 :::

eqs = '''

Im = g_L * (E_L-v) + g_Na * m**3 * h * (E_Na-v) + g_K * n**4 * (E_K-v) : amp/meter**2

dm/dt = alpham * (1-m) - betam * m : 1
dh/dt = alphah * (1-h) - betah * h : 1
dn/dt = alphan * (1-n) - betan * n : 1

alpham = - 0.32/mV * (v - v_th - 13 * mV) / (exp(-(v - v_th - 13 * mV)/(4 * mV)) - 1) /ms : Hz
betam = 0.28/mV * (v - v_th - 40 * mV) / (exp((v - v_th - 40 * mV)/(5 * mV)) -1) /ms : Hz

alphah = 0.128 * exp(-(v - v_th - 17 * mV) / (18 * mV)) /ms : Hz
betah = 4 / (1 + exp(-(v - v_th - 40 * mV) / (5 * mV))) /ms : Hz

alphan = -0.032 / mV * (v - v_th - 15 * mV) / (exp(-(v - v_th - 15 * mV) / (5 * mV)) - 1) / ms : Hz
betan = 0.5 * exp(-(v - v_th - 10 * mV) / (40 * mV)) /ms : Hz

Is = g_ampa * (E_ampa - v) + g_gaba * (E_gaba - v) + g_nmda * (E_nmda - v) / (1 + beta_v_nmda * exp(-alpha_v_nmda * v) * c_Mg) + I : amp (point current)

dg_ampa/dt = -g_ampa/tau_ampa : siemens
dg_gaba/dt = -g_gaba/tau_gaba : siemens

I : amp
g_Na : siemens / meter**2
g_K : siemens / meter**2	
g_nmda : siemens

'''

eqs_ampa_when_spike = '''
g_ampa += weight_ampa
'''

eqs_gaba_when_spike = '''
g_gaba += weight_gaba
'''

# nmda synapse eq.

eqs_nmda = '''

ds/dt = - (1/tau_nmda_decay)*s + alpha_nmda*x*(1-s) : 1
g_nmda_post = g_nmda_max * s : siemens (summed)
dx/dt = - (1/tau_nmda_rise) * x : 1

'''

eqs_nmda_when_spike = '''
x+=1
'''

# ::: channels :::
''' passive membrane properties '''

Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm

g_L = 1e-4 * siemens * cm ** -2
E_L = -70 * mV


''' voltage-dependent channels '''

E_Na = 58 * mV
E_K = -80 * mV
g_Na = 12 * msiemens / cm**2 # This is the value from (Destexhe,Pare,1999) Some models use 50 ms/cm2 (pyramidal neurons, Wang 1998) or 35 ms/cm2 (interneurons)
							  # Experiments: 1994 - Stuart/Sakmann > 4 mS/cm2
g_K = 7 * msiemens / cm**2  # 7
v_th = - 63 * mV

# ::: synaptic parameters :::
''' reversal potential of synaptic channels '''

E_ampa = 0 * mV
E_nmda = 0 * mV
E_gaba = -75 * mV


''' :: kinetics of synaptic channels :: '''

''' decay constant of ampa & gaba_a '''

tau_ampa = 5 * ms
tau_gaba = 5 * ms

''' nmda : kinetics : with open / closed state eq. '''

tau_nmda_rise = 2 * ms # ~ time to remove glutamate from cleft
tau_nmda_decay = 100 * ms # ~ time to close channel after opening
alpha_nmda = 0.5 / ms # ~ how much glutamate increases probability of channel opening

''' change of conductance of ampa & gaba after spike '''
weight_ampa = 0.5
weight_gaba = 0.5

''' maximal possible conductance of nmda synapse (when all channels are open) '''
g_nmda_max =  1 * nS

''' nmda synapse: Mg2+ block '''

alpha_v_nmda = 0.062 / mV
beta_v_nmda = 1 / 3.57 # 1/mM
c_Mg = 3 # mM # 1.2

''' ::: dendritic Na+ channels density '''

ch_dns = 1.

# ::: number of synapses :::

ns_ampa_tot = 200
gaba_ratio = 0.2
ns_gaba_soma = int(ceil(ns_ampa_tot * gaba_ratio))

ns_nmda_tot = 200

# ::: rate of input firing :::

rate_ampa = 1 * Hz
rate_gaba = 1 * Hz
rate_nmda = 0.5

# ::: correlation of synaptic input :::
jtr = 10. * ms
cG = 0.

# ::: location of synapses on compartments :::

if morphology == 'point':
	ns_ampa = ns_ampa_tot
	ns_nmda = ns_nmda_tot
	weight_ampa = scale_wgh_for_point * weight_ampa  # nS # 0.11 is good for 0.5 synapse and 10.3 Hz rate

if morphology == 'ball+stick':
	ns_ampa = int(ns_ampa_tot / n_cmp)
	ns_nmda = int(ns_nmda_tot / n_cmp)

# ::: scanning main parameters :::
mode = 'simple'	#  simple, detailed
speed = 'long'			# fast, long

chosen_param = 'rate_nmda'		# rate, wgh, ch_dns

ch_dns = 0.5

weight_ampa_gaba = 0.5 # nS

rate_ampa_gaba = 1. # Hz
rate_nmda = 1. # Hz



# ::: arrays for scanning :::
param_to_scan = {}


''' rate_ampa_gaba '''

if morphology == 'point':
	rate_range = 17.

if morphology == 'ball+stick':
	rate_range = 5. # program will scan over 10%, 25%, 50%, 75% and 100% of this value


param_to_scan['rate_ampa_gaba'] = np.array([0.1, 0.25, 0.5, 0.75, 1.]) * rate_range
param_to_scan['rate_ampa_gaba'] = param_to_scan['rate_ampa_gaba'].tolist()

''' weight_ampa_gaba '''

if chosen_param == 'weight_ampa_gaba':

	if morphology == 'ball+stick':

		wgh_list = [0.25, 0.5, 0.75, 1.]  # nS
		param_to_scan['weight_ampa_gaba'] = wgh_list
		custom_rate = np.array([4., 2., 1.3, 1.])  # it was 2 then 1.85

		# wgh_list = [0.75]  # nS
		# param_to_scan['wgh'] = wgh_list
		# rate_wgh = np.array([1.3])  # it was 2 then 1.85


	if morphology == 'point':

		wgh_list = [0.05, 0.1, 0.15, 0.2]  # nS
		param_to_scan['weight_ampa_gaba'] = wgh_list
		custom_rate = np.array([24.5, 11.9, 8, 5.9])  # it was 2 then 1.85

		# wgh_list = [0.2]  # nS
		# param_to_scan['wgh'] = wgh_list
		# rate_wgh = np.array([5.9])  # it was 2 then 1.85


''' ch_dns : Na+ dendritic channel density '''

if chosen_param == 'ch_dns':

	if morphology == 'ball+stick':
		param_to_scan['ch_dns'] = [0., 0.25, 0.5, 0.75, 1.]
		# parameters used for wgh: 0.5 nS and jtr: 15 ms > 10 Hz for noncorrelated
		# rate_ch_dns = np.array([10.3, 8., 4., 2.3, 1.7]) * 1 # [20, 2] (2.5 for 0.75 was too high) (1.6 for 1 was too low)
		#rate_ch_dns = np.array([7, 2., 1, 0.5, 0.25]) # [20, 2] (2.5 for 0.75 was too high) (1.6 for 1 was too low)
		custom_rate =  np.array([ 11.5,   9. ,   4.8 ,   2.5,   2.]) # it was 2 then 1.85

	### Scaling of synpses

	if morphology == 'point':
		param_to_scan['ch_dns'] = [0.]
		custom_rate = np.array([11.5])  # [20, 2] (2.5 for 0.75 was too high) (1.6 for 1 was too low)


''' rate_nmda '''

if chosen_param == 'rate_nmda':

	param_to_scan['rate_nmda'] = [0., 0.2, 0.3, 0.5, 1.]
	custom_rate = np.array([4.8, 4.8, 4.8, 4.8, 4.8])  # [20, 2] (2.5 for 0.75 was too high) (1.6 for 1 was too low)



# local correlation
param_to_scan['rL'] = [0., 0.1, 0.25, 0.5, 0.75, 1.]

# correlation scan
corr_scan = np.arange(0.,1.1,0.1)

# ::: times for scanning : fast/long :::
if speed == 'fast':
	t_sim = 2000 * ms
	n_rep = 1

if speed == 'long':
	t_sim = 20000 * ms
	n_rep = 10

if mode == 'detailed':
	t_sim = 1000 * ms

# ::: creating folder for data :::
date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

if not os.path.exists('../DATA/HH/' + str(date_time) + '_[scan_' + chosen_param + ']_[' + morphology + ']'):
	os.makedirs('../DATA/HH/' + str(date_time) + '_[scan_' + chosen_param + ']_[' + morphology + ']')

main_path = os.getcwd()
os.chdir('../DATA/HH/' + str(date_time) + '_[scan_' + chosen_param + ']_[' + morphology + ']')

# ::: saving parameters :::

saved_param = {'date_time': str(date_time),
			   'morphology': morphology,
			   'diam_soma': diam_soma,
			   'diam_0': diam_0,
			   'diam_f': diam_f,
			   'beta': beta,
			   'length': length,
			   'number_of_comp': n_cmp,
			   'ns_ampa': ns_ampa,
			   'gaba_ratio': gaba_ratio,
			   'chosen_param': chosen_param,
			   'rate_ampa_and_gaba': rate_ampa_gaba,
			   'jitter': jtr,
			   'weight_ampa_gaba': weight_ampa_gaba,
			   'param_to_scan': param_to_scan[chosen_param],
			   'corr_scan': corr_scan
			   }

if chosen_param == 'ch_dns' or chosen_param == 'weight_ampa_gaba' or chosen_param == 'rate_nmda':
	saved_param['custom rate'] = custom_rate
np.save('parameters.npy', saved_param)

# ::: t0 and counter :::
print('\n    *** Chosen parameter: ' + str(chosen_param) + ' ***\n\n')
t0 = time.time()
counter = 0

# ::: + main loop for scanning :::
for param in param_to_scan[chosen_param]:

	print('\n\n    *** ' + str(chosen_param) + ' = ' + str(round(param,4)) + ' *** \n')

	if mode == 'detailed':
		os.makedirs(str(chosen_param) + '_' + str(param))

	globals()[chosen_param] = param

	if chosen_param == 'ch_dns' or chosen_param == 'weight_ampa_gaba' or chosen_param == 'rate_nmda':
		rate_ampa_gaba = custom_rate[counter]

	rate_ampa = rate_ampa_gaba * Hz
	rate_gaba = rate_ampa_gaba * Hz
	rate_nmda = rate_nmda * Hz

	weight_ampa = weight_ampa_gaba * nS
	weight_gaba = weight_ampa_gaba * nS


	corr_frq = [[0, 0]]

	for rG in corr_scan.tolist() * n_rep:

		if chosen_param != 'rL':
			rL = 1

		print('\n\nCorrelation: ' + str(round(rG,2)) + '\n')

		neuron_with_dendrite = SpatialNeuron(morphology = morpho, model = eqs,
											 method = method, dt = dt, Cm = Cm, Ri = Ri)

		neuron_with_dendrite.v = -70 * mV
		neuron_with_dendrite.h = 0.
		neuron_with_dendrite.m = 0.
		neuron_with_dendrite.n = 0.
		neuron_with_dendrite.I = 0.


		neuron_with_dendrite.g_Na[0] = g_Na
		neuron_with_dendrite.g_Na[1:] = ch_dns * g_Na

		neuron_with_dendrite.g_K[0] = g_K
		neuron_with_dendrite.g_K[1:] = ch_dns * g_K



		spt_ampa = spike_trains_hierarch(n_cmp, ns_ampa, rate_ampa/Hz, t_sim/second, rG, rG, jtr/second)

		range_of_compartments = range(n_cmp)

		if morphology == 'ball+stick':
			range_of_compartments = range(1,n_cmp)


		times_all_ampa = []
		compartments_all_ampa = []

		for k in range_of_compartments:
			for i_s in range(ns_ampa):

				times = spt_ampa[k][i_s]
				condition = times > 0.

				times = np.extract(condition, times)
				times_all_ampa = np.concatenate((times_all_ampa, times))

				compartments_all_ampa = np.concatenate((compartments_all_ampa, [int(k)] * len(times)))


		if times_all_ampa.tolist() != []:

			indices_all_ampa = np.array(range(len(times_all_ampa)))
			inp_ampa = SpikeGeneratorGroup(len(times_all_ampa), indices_all_ampa, times_all_ampa * second)
			syn_ampa = Synapses(inp_ampa, neuron_with_dendrite, on_pre = eqs_ampa_when_spike)
			syn_ampa.connect(i = indices_all_ampa.astype(int), j = compartments_all_ampa.astype(int))


		spt_gaba = spike_trains_hierarch(1, ns_gaba_soma, rate_gaba / Hz, t_sim / second, 0, 0, jtr / second)
		times_all_gaba = []
		compartments_all_gaba = []

		for i_s in range(ns_gaba_soma):

			times = spt_gaba[0][i_s]
			condition = times > 0.

			times = np.extract(condition, times)
			times_all_gaba = np.concatenate((times_all_gaba, times))

			compartments_all_gaba = np.concatenate((compartments_all_gaba, [0] * len(times)))

		if times_all_gaba.tolist() != []:

			indices_all_gaba = np.array(range(len(times_all_gaba)))
			inp_gaba = SpikeGeneratorGroup(len(times_all_gaba), indices_all_gaba, times_all_gaba * second)
			syn_gaba = Synapses(inp_gaba, neuron_with_dendrite, on_pre = eqs_gaba_when_spike)
			syn_gaba.connect(i = indices_all_gaba.astype(int), j = compartments_all_gaba.astype(int))

		if times_all_ampa.tolist() == [] or times_all_gaba.tolist() == []:

			continue

		spt_nmda = spike_trains_hierarch(n_cmp, ns_nmda, rate_nmda / Hz, t_sim / second, 0, 0, jtr / second)

		times_all_nmda = []
		compartments_all_nmda = []

		for k in range_of_compartments:
			for i_s in range(ns_nmda):
				times = spt_nmda[k][i_s]
				condition = times > 0.

				times = np.extract(condition, times)
				times_all_nmda = np.concatenate((times_all_nmda, times))

				compartments_all_nmda = np.concatenate((compartments_all_nmda, [int(k)] * len(times)))

		if times_all_nmda.tolist() != []:
			indices_all_nmda = np.array(range(len(times_all_nmda)))
			inp_nmda = SpikeGeneratorGroup(len(times_all_nmda), indices_all_nmda, times_all_nmda * second)
			syn_nmda = Synapses(inp_nmda, neuron_with_dendrite, model=eqs_nmda, on_pre=eqs_nmda_when_spike)
			syn_nmda.connect(i=indices_all_nmda.astype(int), j=compartments_all_nmda.astype(int))

		tr = time.time()
		trace = StateMonitor(neuron_with_dendrite, 'v', record=True)
		run(t_sim, report = 'text')
		print('Time of run: ' + str(time.time() - tr) + 's')


		if mode == 'detailed':

			figure()

			plot(trace.t/second,trace.v[0].T/mV )
			xlabel('time (ms)')
			ylabel('membrane potential (mV)')

			savefig(str(chosen_param) + '_' + str(param) + '/figure_V_t_wgh_' + str(wgh) + '_corr_' + str(rG) +'.png', dpi = 300)


			figure()

			plot(trace.t / second, trace.v[100].T / mV)
			xlabel('time (ms)')
			ylabel('dendritic membrane potential (mV)')

			savefig(str(chosen_param) + '_' + str(param) + '/figure_V_t_dend_wgh_' + str(wgh) + '_corr_' + str(rG) + '.png',
					dpi=300)


			figure()

			contf = contourf(cumsum(neuron_with_dendrite.length) / cm, trace.t / ms, trace.v.T / mV, cmap='YlOrRd', alpha=1.,
							 levels=np.linspace(np.amin(trace.v / mV), np.amax(trace.v / mV), num = 100))

			colorbar(contf)
			xlabel('Position [cm]')
			ylabel('Time [ms]')

			savefig(str(chosen_param) + '_' + str(param) + '/figure_V_t_contour_wgh' + str(wgh) + '_corr_' + str(rG) +'.png', dpi=300)


		# Spike counting

		vT = trace.v[0] /mV
		th = 0.

		df_vT = vT - th
		df_vT_rl = np.roll(df_vT,1)

		th_cr_det = df_vT[1:] * df_vT_rl[1:]
		ind_cr = np.where(th_cr_det < 0.)

		n_sp = float(len(ind_cr[0])) / 2.
		frq = n_sp / t_sim

		print('Frequency of spikes: ' + str(frq))

		corr_frq = np.append(corr_frq, [[rG, frq]], axis=0)


		if morphology == 'ball+stick':

			vT_dend = trace.v[100] /mV

			th_dend = -20.

			df_vT_dend = vT_dend - th_dend
			df_vT_dend_rl = np.roll(df_vT_dend, 1)

			th_cr_det_dend = df_vT_dend[1:] * df_vT_dend_rl[1:]
			ind_cr_dend = np.where(th_cr_det_dend < 0.)

			n_sp_dend = float(len(ind_cr_dend[0])) / 2.
			frq_dend = n_sp_dend / t_sim

			print('Frequency of dendritic spikes: ' + str(frq_dend))









	corr_frq = corr_frq[1:]
	corr_mean_frq = [mean(corr_frq[i:len(corr_frq):len(corr_scan)], axis=0).tolist() for i in range(len(corr_scan))]

	fig_n_sp = figure()

	plot(corr_frq[:, 0], corr_frq[:, 1], 'b+')
	plot(array(corr_mean_frq)[:, 0], array(corr_mean_frq)[:, 1], 'r+')

	savefig('figure_frq_weight_ampa_gaba_' + str(weight_ampa_gaba) + '_jtr_' + str(jtr) + '_rate_ampa_gaba_' + str(
		round(rate_ampa_gaba, 4)) + '_rate_nmda_' + str(rate_nmda/Hz) + '_rL_' + str(rL) + '_ch_dns_' + str(ch_dns) + '.png', dpi=300)
	np.save('corr_frq_weight_ampa_gaba_'  + str(weight_ampa_gaba) + '_jtr_' + str(jtr) + '_rate_ampa_gaba_'+ str(
		round(rate_ampa_gaba, 4)) + '_rate_nmda_' + str(rate_nmda/Hz) +  '_rL_' + str(rL) + '_ch_dns_' + str(ch_dns) + '.npy', corr_frq)

	counter = counter + 1

# ::: calculating time of computation :::
t_cmp = (time.time() - t0) / 3600.

print('Time of computation: ' + str(t_cmp) + ' hours')

n_runs = len(param_to_scan[chosen_param] * n_rep * len(corr_scan))

print('For ' + str(n_runs) + ' of ' + str(t_sim/second) + ' s runs \n')
print('Which gives ' + str(round((t_cmp * 3600) / (n_runs * t_sim/second),2)) + ' s of computation for 1 s' )

# ::: loading results from npy : calculation of mean and std :::
corr_frq = {}
corr_frq_mean = {}
corr_frq_std = {}

n_corr = len(corr_scan)
low_plot = 0
high_plot = len(param_to_scan[str(chosen_param)])


maxT = []
counter = 0

for param in param_to_scan[chosen_param]:

    if chosen_param == 'ch_dns' or chosen_param == 'weight_ampa_gaba' or chosen_param == 'rate_nmda' :
        rate_ampa_gaba = custom_rate[counter]

    globals()[chosen_param] = param

    corr_frq[str(param)] = np.load('corr_frq_weight_ampa_gaba_'  + str(weight_ampa_gaba) + '_jtr_' + str(jtr) + '_rate_ampa_gaba_' + str(
		round(rate_ampa_gaba,3)) + '_rate_nmda_' + str(rate_nmda) + '_rL_' + str(rL) + '_ch_dns_' + str(ch_dns) + '.npy')
    corr_list = corr_frq[str(param)][:,0]

    bool_corr = {}
    corr_frq_red = {}

    all_mean = [[0,0]]
    all_std = [[0,0]]

    for rG in corr_scan:

        bool_corr[str(rG)] = np.where(corr_list == np.ones(len(corr_list)) * rG)[0]

        corr_frq_red[str(rG)] = corr_frq[str(param)][bool_corr[str(rG)]]

        mean = np.mean(corr_frq_red[str(rG)], axis = 0)
        std = np.std(corr_frq_red[str(rG)], axis = 0)

        all_mean = np.append(all_mean, [mean],axis = 0)
        all_std = np.append(all_std, [std],axis = 0)

    corr_frq_mean[str(param)] = all_mean[1:]
    corr_frq_std[str(param)] = all_std[1:]

    meanT = np.array(corr_frq_mean[str(param)])[:,1]

    mean_without_nan = meanT[~np.isnan(meanT)]

    maxT = np.append(maxT, np.amax(mean_without_nan))

    counter = counter + 1


maxmaxT = np.amax(maxT)

# ::: plots :::
import matplotlib.pyplot as plt
import matplotlib.cm as cm

color_array = np.linspace(0.3, 1, num=len(param_to_scan[chosen_param]))
color_array = color_array.tolist()
#color_array_reversed = color_array.reverse()

param_to_scan[chosen_param] = param_to_scan[chosen_param][low_plot:high_plot]

# labels
labels = array(param_to_scan[chosen_param])

if chosen_param == 'rate_per_weight':
    labels = array(param_to_scan[chosen_param]) * wgh


fig_corr = plt.figure(figsize=(12,7))

ax = fig_corr.add_subplot(111)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.9, box.height])

ax.set_ylim(bottom = 0, top = 1.2 * maxmaxT )
ax.set_xlabel('ratio of shared spikes', fontsize = 24)
ax.set_ylabel('firing rate [Hz]', fontsize = 24)


clr = 0

for param in param_to_scan[chosen_param]:

    globals()[chosen_param] = param

    c = cm.YlOrRd(color_array[clr], 1)

    upper_limit = np.array(corr_frq_mean[str(param)])[:,1] + np.array(corr_frq_std[str(param)])[:,1]
    lower_limit = np.array(corr_frq_mean[str(param)])[:,1] - np.array(corr_frq_std[str(param)])[:,1]

    ax.plot(np.array(corr_frq_mean[str(param)])[:,0], np.array(corr_frq_mean[str(param)])[:,1], color = c, label = labels[clr])
    ax.fill_between(np.array(corr_frq_mean[str(param)])[:,0], lower_limit, upper_limit, color = c, alpha = 0.5)

    leg = plt.legend()
    # get the individual lines inside legend and set line width
    for line in leg.get_lines():
     	line.set_linewidth(4)

    clr = clr + 1


ax.legend(loc = 'center left', bbox_to_anchor = (1.0, 0.5), frameon = False, fontsize = 22)


plt.savefig('[scan_' + chosen_param + '].png', dpi = 300)
plt.savefig('[scan_' + chosen_param + '].svg', dpi = 300)

# ::: saving final results :::
np.save('chosen_param.npy', chosen_param)
np.save('param_to_scan.npy', param_to_scan[chosen_param])
np.save('corr_frq_mean.npy', corr_frq_mean)
np.save('corr_frq_std.npy', corr_frq_std)

os.chdir(main_path)