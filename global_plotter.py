
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os
import time

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"



os.chdir('../DATA/HH/18-08-03--16-58-27_[scan_wgh]_[point] ***')


# ball + stick : different channel densities

corr_frq_mean_bs = np.load('corr_frq_mean.npy')
corr_frq_std_bs = np.load('corr_frq_std.npy')

corr_frq_mean_bs = corr_frq_mean_bs.item()
corr_frq_std_bs = corr_frq_std_bs.item()


print(corr_frq_mean_bs)
print(corr_frq_std_bs)

# os.chdir('../18-07-30--16-28-20_[scan_ch_dns]_[point] *** [jtr: 10 ms] [fr: 15 Hz]')
#
# corr_frq_mean_p = np.load('corr_frq_mean.npy')
# corr_frq_std_p = np.load('corr_frq_std.npy')
#
# corr_frq_mean_p = corr_frq_mean_p.item()
# corr_frq_std_p = corr_frq_std_p.item()
#
#
# print(corr_frq_mean_p)
# print(corr_frq_std_p)


os.chdir('../')

date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

os.makedirs(str(date_time) + '_[plot_merged]')

os.chdir(str(date_time) + '_[plot_merged]')


param_L = list(corr_frq_mean_bs.keys())

param_p = list(corr_frq_mean_bs.keys())[0]

labels = np.array(param_L)


clr = 0

color_array = np.linspace(0.2, 0.6, num=(len(param_L)+1))
color_array = color_array.tolist()
color_array_reversed = color_array.reverse()

c = cm.pink(color_array, 1)

print(color_array)


fig_corr = plt.figure(figsize=(12,7))

ax = fig_corr.add_subplot(111)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.9, box.height])

ax.set_ylim(bottom = 0, top = 45)
ax.set_xlim(-0.01,1.05)
ax.set_xlabel('ratio of shared spikes', fontsize = 24)
ax.set_ylabel('firing rate [Hz]', fontsize = 24)



# upper_limit = np.array(corr_frq_mean_p[str(param_p)])[:,1] + np.array(corr_frq_std_bs[str(param_p)])[:,1]
# lower_limit = np.array(corr_frq_mean_p[str(param_p)])[:,1] - np.array(corr_frq_std_bs[str(param_p)])[:,1]
#
# ax.plot(np.array(corr_frq_mean_p[str(param_p)])[:, 0], np.array(corr_frq_mean_p[str(param_p)])[:, 1], color= c[0], alpha = 0.6)
# ax.fill_between(np.array(corr_frq_mean_p[str(param_p)])[:, 0], lower_limit, upper_limit, color= c[0], alpha=0.5, hatch="X", label = 'point')

clr = 1

for param in param_L:

    upper_limit = np.array(corr_frq_mean_bs[str(param)])[:,1] + np.array(corr_frq_std_bs[str(param)])[:,1]
    lower_limit = np.array(corr_frq_mean_bs[str(param)])[:,1] - np.array(corr_frq_std_bs[str(param)])[:,1]

    ax.plot(np.array(corr_frq_mean_bs[str(param)])[:,0], np.array(corr_frq_mean_bs[str(param)])[:,1], color = c[clr], alpha = 0.6)
    ax.fill_between(np.array(corr_frq_mean_bs[str(param)])[:,0], lower_limit, upper_limit, color = c[clr], alpha = 0.4, label = str(round(float(labels[clr-1]),2)))

    clr = clr + 1




leg = ax.legend(loc = 'center left', bbox_to_anchor = (1.0, 0.5), frameon = False, fontsize = 22)

for line in leg.get_lines():
    line.set_linewidth(10)

plt.savefig('final.png', dpi = 300)
plt.savefig('final.svg', dpi = 300)


plt.show()

























