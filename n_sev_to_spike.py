

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "12"



dist = np.arange(100,1001,100)

n_sev_hh = [17,10,7,6,5,4,4,3,2,2]
n_sev_ae = [20,11,8,7,6,5,5,4,3,2]




plt.figure()
plt.plot(dist,n_sev_hh, '+''k')


fig, ax = plt.subplots()

for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_locator(ticker.MaxNLocator(integer=True))


ax.plot(dist,n_sev_ae, '+''k')

plt.xlabel('distance from soma [$\mu$m]')
plt.ylabel('number of synaptic events to initiate d-spike')

plt.savefig('../DATA/n_sev_to_spike_hh.png', dpi = 300)
plt.savefig('../DATA/n_sev_to_spike_hh.svg', dpi = 300)

plt.show()




plt.figure()
plt.plot(dist,n_sev_ae, '+''k')

fig, ax = plt.subplots()

for axis in [ax.xaxis, ax.yaxis]:
    axis.set_major_locator(ticker.MaxNLocator(integer=True))

ax.plot(dist,n_sev_ae, '+''k')

plt.xlabel('distance from soma [$\mu$m]')
plt.ylabel('number of synaptic events to initiate d-spike')

plt.ylim(0,22)

plt.savefig('../DATA/n_sev_to_spike_ae.png', dpi = 300)
plt.savefig('../DATA/n_sev_to_spike_ae.svg', dpi = 300)

plt.show()


