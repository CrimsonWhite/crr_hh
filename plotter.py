import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"

os.chdir('../DATA/HH/18-07-26--18-11-49_[scan_ch_dns]_[ball+stick]')


saved_param = np.load('parameters.npy')

chosen_param = saved_param.item()['chosen_param']
rate_per_weight = saved_param.item()['rate_per_weight']
jtr = saved_param.item()['jitter']
wgh = saved_param.item()['wgh']
param_to_scan = saved_param.item()['param_to_scan']
corr_scan = saved_param.item()['corr_scan']
rL = 1


rate_ch_dns = np.array([10.3, 8., 4., 2.3, 1.7])  # [20, 2] (2.5 for 0.75 was too high) (1.6 for 1 was too low)



corr_frq = {}
corr_frq_mean = {}
corr_frq_std = {}


n_corr = len(corr_scan)

low_plot = 0
high_plot = len(param_to_scan)


maxT = []

counter = 0

for param in param_to_scan:

    if chosen_param == 'ch_dns':
        rate_per_weight = rate_ch_dns[counter]

    globals()[chosen_param] = param

    corr_frq[str(param)] = np.load('corr_frq_weight_'  + str(wgh) + '_jtr_' + str(jtr) + '_rate_per_weight_' + str(
		round(rate_per_weight,3)) + '_rL_' + str(rL) + '_ch_dns_' + str(ch_dns) + '.npy')
    corr_list = corr_frq[str(param)][:,0]

    bool_corr = {}
    corr_frq_red = {}

    all_mean = [[0,0]]
    all_std = [[0,0]]

    for rG in corr_scan:

        bool_corr[str(rG)] = np.where(corr_list == np.ones(len(corr_list)) * rG)[0]

        corr_frq_red[str(rG)] = corr_frq[str(param)][bool_corr[str(rG)]]

        mean = np.mean(corr_frq_red[str(rG)], axis = 0)
        std = np.std(corr_frq_red[str(rG)], axis = 0)

        all_mean = np.append(all_mean, [mean],axis = 0)
        all_std = np.append(all_std, [std],axis = 0)

    corr_frq_mean[str(param)] = all_mean[1:]
    corr_frq_std[str(param)] = all_std[1:]

    meanT = np.array(corr_frq_mean[str(param)])[:,1]

    mean_without_nan = meanT[~np.isnan(meanT)]

    maxT = np.append(maxT, np.amax(mean_without_nan))

    counter = counter + 1

maxmaxT = np.amax(maxT)

color_array = np.linspace(0.3, 1, num=len(param_to_scan))
color_array = color_array.tolist()
color_array_reversed = color_array.reverse()


param_to_scan = param_to_scan[low_plot:high_plot]



# labels

labels = np.array(param_to_scan)

if chosen_param == 'rate_per_weight':
    labels = np.array(param_to_scan) * wgh


fig_corr = plt.figure(figsize=(12,7))

ax = fig_corr.add_subplot(111)
box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width*0.9, box.height])

ax.set_ylim(bottom = 0, top = 1.2 * maxmaxT )
ax.set_xlabel('ratio of shared spikes', fontsize = 24)
ax.set_ylabel('firing rate [Hz]', fontsize = 24)


clr = 0

for param in param_to_scan:

    globals()[chosen_param] = param

    c = cm.YlOrRd(color_array[clr], 1)

    upper_limit = np.array(corr_frq_mean[str(param)])[:,1] + np.array(corr_frq_std[str(param)])[:,1]
    lower_limit = np.array(corr_frq_mean[str(param)])[:,1] - np.array(corr_frq_std[str(param)])[:,1]

    ax.plot(np.array(corr_frq_mean[str(param)])[:,0], np.array(corr_frq_mean[str(param)])[:,1], color = c, label = labels[clr])
    ax.fill_between(np.array(corr_frq_mean[str(param)])[:,0], lower_limit, upper_limit, color = c, alpha = 0.5)

    leg = plt.legend()
    # get the individual lines inside legend and set line width
    for line in leg.get_lines():
     	line.set_linewidth(4)

    clr = clr + 1


ax.legend(loc = 'center left', bbox_to_anchor = (1.0, 0.5), frameon = False, fontsize = 22)


plt.savefig('[scan_' + chosen_param + '].png', dpi = 300)
plt.savefig('[scan_' + chosen_param + '].svg', dpi = 300)

