

import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.cm as cm



plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"


os.chdir('/home/white/Neuro/dendrites/paper - correlation processing/v3/new_fig/EPSPs')

distanceT = np.load('distance.npy')

EPSPs_soma_HH = np.load('EPSPS at soma HH.npy')
EPSPs_local_HH = np.load('local depolarization HH.npy')

EPSPs_soma_IF = np.load('EPSPS at soma IF.npy')
EPSPs_local_IF = np.load('local depolarization IF.npy')



c1 = cm.YlOrRd(0.65, 1)
c2 = cm.YlOrRd(0.9, 1)


plt.figure(figsize=(12,7))

plt.plot(distanceT, EPSPs_soma_HH, color = c1, linewidth=2)
plt.plot(distanceT, EPSPs_soma_IF, ':', color = c2, linewidth=2)




plt.xlabel('distance from soma [$\mu$m]')
plt.ylabel('depolarization at soma [mV]')

plt.savefig('EPSPs at soma.png', dpi = 300)
plt.savefig('EPSPs at soma.svg', dpi = 300)

plt.show()




plt.figure(figsize=(12,7))

plt.plot(distanceT, EPSPs_local_HH, color = c1, linewidth=2)
plt.plot(distanceT, EPSPs_local_IF, ':', color = c2, linewidth=2)


plt.xlabel('distance from soma [$\mu$m]')
plt.ylabel('local depolarization [mV]')

plt.savefig('EPSPs local.png', dpi = 300)
plt.savefig('EPSPs local.svg', dpi = 300)

plt.show()

