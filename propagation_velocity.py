from brian2 import *
from inputs import *
import os
import time
import numpy

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"


# BrianLogger.suppress_hierarchy('brian2.codegen')
# BrianLogger.suppress_name('resolution_conflict')
# BrianLogger.suppress_hierarchy('brian2')


# passive
Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm
g_l = 1e-4 * siemens * cm ** -2
E_l = -70 * mV

# hh
E_Na = 58 * mV
E_K = -80 * mV
g_Na = 12 * msiemens / cm ** 2
g_K = 7 * msiemens / cm ** 2
v_th = - 63 * mV

# synaptic
E_ampa = 0 * mV
E_gaba = -75 * mV

tau_ampa = 5 * ms
tau_gaba = 5 * ms

dg_ampa = 1 * nS
dg_gaba = 1 * nS

### HERE PARAMETERS TO CHANGE ###

t_sim = 1000 * ms
wgh = 10. # 0.5

stim_type = 'exact_events' # exact_events, noise

event_type = 'creation'

# creation

if event_type == 'creation':

    indices = array([0])
    times = array([10]) * ms  # 5 ms
    compartments = array([0])

if event_type == 'EPSP_to_spike':

    n_sev = 16
    distance_from_soma = 50
    compartment_stim = int((distance_from_soma / 1000) * 200)

    indices = np.arange(0,n_sev)
    times = array(n_sev*[0.05]) * ms  # 5 ms
    compartments = array(n_sev*[compartment_stim])

if event_type == 'refractoriness':

    indices = array([0,1])
    times = array([11., 14.9]) * ms  # 5 ms
    compartments = array([200, 250])

    tau_ampa = 0.5 * ms
    tau_gaba = 0.5 * ms

    dg_ampa = 6 * nS
    dg_gaba = 6 * nS


# refractoriness


rate_per_weight = 5  # 0.8
jtr = 10. * ms
cG = 0.

ch_dns = 1.


# visuailzation

plot_v_profiles = 'no'

v_pr_amp = 1./5. # 1./7.
t_window = 10 * ms # 15 ms
tp_ds = 1

# morphology
morphology = 'ball+stick'  # point, ball+stick
dend_shape = 'cone'  # cylinder or cone

### NUMERICAL
method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'
dt = 0.01 * ms  # 0.005

### MORPHOLOGY

diam_soma = 40 * um  # 40

# length of dendrite
length = 1000 * um # 1000 !!!
len_comp = 5 * um  # 5 !!!
n_cmp = int(length / um / (len_comp / um))

# cylinder
diam_cln = 1 * um

# cone
diam_0 = 5
diam_f = 0.5 # 0.25 in AdEx, 0.5 on default here
beta = 0.05  # smaller more steep decrease of diameter 0.1

lengthT = linspace(0, length / um, num=n_cmp + 1)

### SYNAPTIC

gaba_ratio = 0.2  # 0.25

if morphology == 'point':
    ns_ampa = 200
    ns_ampa_noise = 200
    ns_gaba_soma = int(ceil(ns_ampa * gaba_ratio))

if morphology == 'ball+stick':
    ns_ampa = 1
    ns_ampa_noise = 1
    ns_gaba_soma = 40


if morphology == 'ball+stick':

    morpho = Soma(diameter=diam_soma)

    if dend_shape == 'cylinder':
        morpho.dendrite = Cylinder(diameter=diam_cln, length=length, n=n_cmp)

    if dend_shape == 'cone':
        alpha = (diam_0 - diam_f) / ((length / um) ** beta)

        diameterT = - alpha * lengthT ** beta + diam_0

        morpho.dendrite = Section(diameter=diameterT * um, length=[len_comp / um] * n_cmp * um, n=n_cmp)

if morphology == 'soma':
    length = diam_soma
    n_cmp = 1

    morpho = Soma(diameter=diam_soma)

# there is an error (Magic Network) when there are no spikes


date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

if not os.path.exists(
        '../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']'):
    os.makedirs(
        '../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']')

main_path = os.getcwd()
os.chdir('../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']')

saved_param = {'date_time': str(date_time),
               'morphology': morphology,
               'diam_soma': diam_soma,
               'diam_0': diam_0,
               'diam_f': diam_f,
               'beta': beta,
               'length': length,
               'number_of_comp': n_cmp,
               'ns_ampa': ns_ampa,
               'gaba_ratio': gaba_ratio,
               'rate_per_weight': rate_per_weight,
               'jitter': jtr,
               'wgh': wgh,
}

np.save('parameters.npy', saved_param)

t0 = time.time()



weight_ampa = wgh
weight_gaba = wgh

rate_ampa = rate_per_weight / weight_ampa * Hz
rate_gaba = rate_per_weight / weight_gaba * Hz

# parameters from Pare 1998

eqs = """

Im = g_l * (E_l-v) + g_Na * m**3 * h * (E_Na-v) + g_K * n**4 * (E_K-v) : amp/meter**2

dm/dt = alpham * (1-m) - betam * m : 1
dh/dt = alphah * (1-h) - betah * h : 1
dn/dt = alphan * (1-n) - betan * n : 1


alpham = - 0.32/mV * (v - v_th - 13 * mV) / (exp(-(v - v_th - 13 * mV)/(4 * mV)) - 1) /ms : Hz
betam = 0.28/mV * (v - v_th - 40 * mV) / (exp((v - v_th - 40 * mV)/(5 * mV)) -1) /ms : Hz

alphah = 0.128 * exp(-(v - v_th - 17 * mV) / (18 * mV)) /ms : Hz
betah = 4 / (1 + exp(-(v - v_th - 40 * mV) / (5 * mV))) /ms : Hz

alphan = -0.032 / mV * (v - v_th - 15 * mV) / (exp(-(v - v_th - 15 * mV) / (5 * mV)) - 1) / ms : Hz
betan = 0.5 * exp(-(v - v_th - 10 * mV) / (40 * mV)) /ms : Hz

Is = g_ampa * (E_ampa - v) + g_gaba * (E_gaba - v) + I : amp (point current)

dg_ampa/dt = -g_ampa/tau_ampa : siemens
dg_gaba/dt = -g_gaba/tau_gaba : siemens

I : amp
g_Na : siemens / meter**2
g_K : siemens / meter**2


"""


neuron = SpatialNeuron(morphology=morpho, model=eqs, method=method, dt=dt, Cm=Cm, Ri=Ri)

neuron.v = -70 * mV
neuron.h = 0.
neuron.m = 0.
neuron.n = 0.
neuron.I = 0.

neuron.g_Na[0] = g_Na
neuron.g_Na[1:] = ch_dns * g_Na

neuron.g_K[0] = g_K
neuron.g_K[1:] = ch_dns * g_K





trace = StateMonitor(neuron, 'v', record=True)


if stim_type == 'noise':

    spt_exc = spike_trains_hierarch(n_cmp, ns_ampa, rate_ampa / Hz, t_sim / second, cG, cG, jtr / second)

    range_of_compartments = range(n_cmp)

    if morphology == 'ball+stick':
        range_of_compartments = range(1, n_cmp)

    times_all_exc = []
    compartments_all_exc = []

    for k in range_of_compartments:

        for i_s in range(ns_ampa):
            times = spt_exc[k][i_s]

            condition = times > 0.

            times = np.extract(condition, times)

            times_all_exc = np.concatenate((times_all_exc, times))

            compartments_all_exc = np.concatenate((compartments_all_exc, [int(k)] * len(times)))

    if times_all_exc.tolist() != []:
        indices_all_exc = np.array(range(len(times_all_exc)))

        inp_exc = SpikeGeneratorGroup(len(times_all_exc), indices_all_exc, times_all_exc * second)

        syn_exc = Synapses(inp_exc, neuron, on_pre='g_ampa += ' + str(weight_ampa) + '* dg_gaba')

        syn_exc.connect(i=indices_all_exc.astype(int), j=compartments_all_exc.astype(int))

    # spt_inh = spike_trains_hierarch_ind_global(n_cmp, ns_gaba, rate_gaba/Hz, t_sim/second, rL, rG, jtr/second)
    spt_inh = spike_trains_hierarch(1, ns_gaba_soma, rate_gaba / Hz, t_sim / second, 0., 0., jtr / second)

    times_all_inh = []
    compartments_all_inh = []

    for i_s in range(ns_gaba_soma):
        times = spt_inh[0][i_s]
        condition = times > 0.

        times = np.extract(condition, times)
        times_all_inh = np.concatenate((times_all_inh, times))

        compartments_all_inh = np.concatenate((compartments_all_inh, [0] * len(times)))

    if times_all_inh.tolist() != []:
        indices_all_inh = np.array(range(len(times_all_inh)))
        inp_inh = SpikeGeneratorGroup(len(times_all_inh), indices_all_inh, times_all_inh * second)
        syn_inh = Synapses(inp_inh, neuron, on_pre='g_gaba += ' + str(weight_gaba) + '* dg_gaba')
        syn_inh.connect(i=indices_all_inh.astype(int), j=compartments_all_inh.astype(int))


tr = time.time()


if stim_type == 'exact_events':

    input = SpikeGeneratorGroup(len(times), indices, times)
    syn_exc = Synapses(input, neuron, on_pre='g_ampa += ' + str(weight_ampa) + '* dg_ampa')
    syn_exc.connect(i=indices.astype(int), j=compartments.astype(int))



run(t_sim, report='text')
print('Time of run: ' + str(time.time() - tr) + 's')



vT = trace.v/mV
th = 0.

spike_time = []

for i in range(0,n_cmp+1):

    df_vT = vT[i] - th
    df_vT_rl = np.roll(df_vT, 1)

    th_cr_det = df_vT[1:] * df_vT_rl[1:]
    ind_cr = np.where(th_cr_det < 0.)

    spike_time.append(ind_cr[0][1])

print(spike_time)

print(len(spike_time))

vel_T = []
d_from_soma_T = []

for i in range(18):

    vel = (10*len_comp /um) / ((spike_time[10*i+10] - spike_time[10*i])/100)

    vel_T.append(vel)

    d_from_soma_T.append((10*i + 5) * len_comp/um)


print(vel_T)

figure(figsize=(12, 7))

xlabel('distance from soma [$\mu$m]')
ylabel('velocity of dendritic sodium spike [$\mu$m/ms]')

plt.plot(d_from_soma_T,vel_T, 'k+', markersize=15, linewidth=2)
# plt.plot(d_from_soma_T,vel_T, 'k', linewidth=2)


savefig('velocity.png', dpi = 300)
savefig('velocity.svg', dpi = 300)
plt.show()


# diameter

figure(figsize=(12, 10))

plot(lengthT, diameterT,'k')
xlabel('distance from soma [$\mu$m]')
ylabel('dendritic diameter [$\mu$m]')

savefig('dendrite_diameter.png', dpi = 300)
savefig('dendrite_diameter.svg', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[0].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_soma.png', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[100].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_dendrite_middle.png', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[200].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_dendrite_end.png', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[50].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')
plt.xlim(10,100)

savefig('v_dendrite_beginning.png', dpi = 300)






# propagation: heat map

figure(figsize=(12, 7))

contf = contourf(cumsum(neuron.length) / um, trace.t / ms, trace.v.T / mV, cmap='YlOrRd',
                 alpha=1., levels=np.linspace(np.amin(trace.v / mV), np.amax(trace.v / mV), num=100))
colorbar(contf)
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)


# propagation: voltage profiles


if plot_v_profiles == 'yes':

    for t_pl_0_ul in np.arange(0, t_sim/ms-15, t_window/ms):

        t_pl_0 = t_pl_0_ul * ms
        t_pl = t_window

        tp_pl_0 = int(t_pl_0/dt)
        tp_pl = int(t_pl/dt)


        figure(figsize=(10, 15))

        for i in range(tp_pl_0, tp_pl_0 + tp_pl, tp_ds):
            plot(cumsum(neuron.length) / um, dt/ms * i + dt/ms * v_pr_amp * (trace.v[:,i].T / mV - 70.), 'k')

        ylabel('time [ms]', fontsize = 26)
        xlabel('distance from soma [$\mu$m]', fontsize = 26)

        savefig('propagations_voltage_profile_' + str(int(t_pl_0/ms)).rjust(3, '0') + '.png', dpi = 300)





# for t_pl_0_ul in np.arange(0, t_sim/ms, t_window/ms):
#
#     t_pl_0 = t_pl_0_ul * ms
#     t_pl = t_window
#
#     tp_pl_0 = int(t_pl_0/dt)
#     tp_pl = int(t_pl/dt)
#
#     figure(figsize=(10, 15))
#
#     contourf(cumsum(neuron.length) / um, trace.t[tp_pl_0 : tp_pl_0 + tp_pl]/ms, trace.v[:,tp_pl_0 : tp_pl_0 + tp_pl].T/mV, cmap='YlOrRd',
#              alpha=1., levels=np.linspace(np.amin(trace.v[:,tp_pl_0 : tp_pl_0 + tp_pl] / mV), np.amax(trace.v[:,tp_pl_0 : tp_pl_0 + tp_pl] / mV), num=100))
#
#     ylabel('time [ms]', fontsize = 26)
#     xlabel('distance from soma [$\mu$m]', fontsize = 26)
#
#     savefig('propagations_voltage_color_map' + str(t_pl_0) + '.png', dpi = 300)

