from brian2 import *
from inputs import *
import os
import time
import numpy

plt.rcParams["font.family"] = "sans-serif"
plt.rcParams['font.sans-serif'] = 'Museo Sans'
plt.rcParams["font.size"] = "22"


BrianLogger.suppress_hierarchy('brian2.codegen')
BrianLogger.suppress_name('resolution_conflict')
BrianLogger.suppress_hierarchy('brian2')




# ::: time of computation :::
t_sim = 1000 * ms

# ::: numerical parameters of simulation :::

dt = 0.05 * ms  # 0.005
method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'

# ::: choosing morphology :::

morphology = 'ball+stick'  # point, ball+stick
dend_shape = 'cone'  # cylinder or cone

# ::: defining morphology :::

""" soma diameter """
diam_soma = 40 * um  # 40

""" length of dendrite """
length = 1000 * um # 1000 !!!
len_comp = 5 * um  # 5 !!!
n_cmp = int(length / um / (len_comp / um))

""" cylinder """
diam_cln = 1 * um

""" cone """
diam_0 = 5
diam_f = 0.5 # 0.25 in AdEx, 0.5 on default here
beta = 0.05  # smaller more steep decrease of diameter 0.1

lengthT = linspace(0, length / um, num=n_cmp + 1)

# ::: construction of morphology :::

if morphology == 'point':
	length = diam_soma
	n_cmp = 1
	morpho = Soma(diameter = diam_soma)


if morphology == 'ball+stick':
	morpho = Soma(diameter=diam_soma)

	if dend_shape == 'cylinder':
		morpho.dendrite = Cylinder(diameter=diam_cln, length=length, n=n_cmp)

	if dend_shape == 'cone':

		alpha = (diam_0 - diam_f) / ((length / um) ** beta)
		diameterT = - alpha * lengthT ** beta + diam_0
		morpho.dendrite = Section(diameter=diameterT * um, length=[len_comp / um] * n_cmp * um, n=n_cmp)

# ::: differential eqs. :::   ::: parameters from Pare 1998 :::

eqs = '''

Im = g_L * (E_L-v) + g_Na * m**3 * h * (E_Na-v) + g_K * n**4 * (E_K-v) : amp/meter**2

dm/dt = alpham * (1-m) - betam * m : 1
dh/dt = alphah * (1-h) - betah * h : 1
dn/dt = alphan * (1-n) - betan * n : 1

alpham = - 0.32/mV * (v - v_th - 13 * mV) / (exp(-(v - v_th - 13 * mV)/(4 * mV)) - 1) /ms : Hz
betam = 0.28/mV * (v - v_th - 40 * mV) / (exp((v - v_th - 40 * mV)/(5 * mV)) -1) /ms : Hz

alphah = 0.128 * exp(-(v - v_th - 17 * mV) / (18 * mV)) /ms : Hz
betah = 4 / (1 + exp(-(v - v_th - 40 * mV) / (5 * mV))) /ms : Hz

alphan = -0.032 / mV * (v - v_th - 15 * mV) / (exp(-(v - v_th - 15 * mV) / (5 * mV)) - 1) / ms : Hz
betan = 0.5 * exp(-(v - v_th - 10 * mV) / (40 * mV)) /ms : Hz

Is = g_ampa * (E_ampa - v) + g_gaba * (E_gaba - v) + g_nmda * (E_nmda - v) / (1 + beta_v_nmda * exp(-alpha_v_nmda * v) * c_Mg) + I : amp (point current)

dg_ampa/dt = -g_ampa/tau_ampa : siemens
dg_gaba/dt = -g_gaba/tau_gaba : siemens

I : amp
g_Na : siemens / meter**2
g_K : siemens / meter**2	
g_nmda : siemens

'''

# nmda synapse eq.

eqs_nmda = '''

ds/dt = - (1/tau_nmda_decay)*s + alpha_nmda*x*(1-s) : 1
g_nmda_post = g_nmda_max * s : siemens (summed)
dx/dt = - (1/tau_nmda_rise) * x : 1

'''

eqs_nmda_when_spike = '''
x+=1
'''

# ::: channels :::
""" passive membrane properties """

Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm

g_L = 1e-4 * siemens * cm ** -2
E_L = -70 * mV


""" voltage-dependent channels """

E_Na = 58 * mV
E_K = -80 * mV
g_Na = 12 * msiemens / cm**2 # This is the value from (Destexhe,Pare,1999) Some models use 50 ms/cm2 (pyramidal neurons, Wang 1998) or 35 ms/cm2 (interneurons)
							  # Experiments: 1994 - Stuart/Sakmann > 4 mS/cm2
g_K = 7 * msiemens / cm**2  # 7
v_th = - 63 * mV

# ::: synaptic :::

""" reversal potential of synaptic channels """

E_ampa = 0 * mV
E_nmda = 0 * mV
E_gaba = -75 * mV


""" :: kinetics of synaptic channels :: """

""" decay constant of ampa & gaba_a """

tau_ampa = 5 * ms
tau_gaba = 5 * ms


""" nmda : kinetics : with open / closed state eq. """

tau_nmda_rise = 2 * ms # ~ time to remove glutamate from cleft
tau_nmda_decay = 100 * ms # ~ time to close channel after opening
alpha_nmda = 0.5 / ms # ~ how much glutamate increases probability of channel opening


""" change of conductance of ampa & gaba """

weight_ampa = 0.5
weight_gaba = 0.5


""" maximal possible conductance of nmda synapse (when all channels are open) """

g_nmda_max =  1 * nS


""" nmda synapse: Mg2+ block """

alpha_v_nmda = 0.062 / mV
beta_v_nmda = 1 / 3.57 # 1/mM
c_Mg = 3 # mM # 1.2

""" ::: dendritic Na+ channels density """

ch_dns = 0.5


""" synaptic scaling for point neuron to get the same output as for passive dendrite """

scale_wgh_for_point = 0.105/0.5

# ::: number of synapses :::

ns_ampa_tot = 200
gaba_ratio = 0.2
ns_gaba_soma = int(ceil(ns_ampa_tot * gaba_ratio))

ns_nmda_tot = 200

# ::: rate of input firing :::

rate_ampa = 1 * Hz
rate_gaba = 1 * Hz
rate_nmda = 0.1 * Hz

# ::: correlation of synaptic input :::
jtr = 10. * ms
cG = 0.

# ::: location of synapses on compartments :::

if morphology == 'point':
	ns_ampa = ns_ampa_tot
	ns_nmda = ns_nmda_tot
	weight_ampa = scale_wgh_for_point * weight_ampa  # nS # 0.11 is good for 0.5 synapse and 10.3 Hz rate

if morphology == 'ball+stick':
	ns_ampa = int(ns_ampa_tot / n_cmp)
	ns_nmda = int(ns_nmda_tot / n_cmp)

# ::: creating neuron :::

neuron = SpatialNeuron(morphology=morpho, model=eqs, method=method, dt=dt, Cm=Cm, Ri=Ri)

neuron.v = -70 * mV
neuron.h = 0.
neuron.m = 0.
neuron.n = 0.
neuron.I = 0.

neuron.g_Na[0] = g_Na
neuron.g_Na[1:] = ch_dns * g_Na

neuron.g_K[0] = g_K
neuron.g_K[1:] = ch_dns * g_K

# ::: type of stimulation :::   ::: creation of synapses :::

stim_type = 'noise'

# creation of sodium spike in the center of dendrite
if stim_type == 'creation':

    indices = array([0])
    times = array([0.05]) * ms  # 5 ms
    compartments = array([150])

# how many synaptic inputs is needed to initiate Na+ dendrtic spike

if stim_type == 'EPSP_to_spike':

    n_sev = 16
    distance_from_soma = 50
    compartment_stim = int((distance_from_soma / 1000) * 200)

    indices = np.arange(0,n_sev)
    times = array(n_sev*[0.05]) * ms  # 5 ms
    compartments = array(n_sev*[compartment_stim])

# effect of refractoriness
if stim_type == 'refractoriness':

    indices = array([0,1])
    times = array([11., 14.9]) * ms  # 5 ms
    compartments = array([200, 250])

# correlated poissonian synaptic input
if stim_type == 'noise':


    spt_ampa = spike_trains_hierarch(n_cmp, ns_ampa, rate_ampa / Hz, t_sim / second, cG, cG, jtr / second)

    range_of_compartments = range(n_cmp)

    if morphology == 'ball+stick':
        range_of_compartments = range(1, n_cmp)

    times_all_ampa = []
    compartments_all_ampa = []

    for k in range_of_compartments:

        for i_s in range(ns_ampa):
            times = spt_ampa[k][i_s]

            condition = times > 0.

            times = np.extract(condition, times)

            times_all_ampa = np.concatenate((times_all_ampa, times))

            compartments_all_ampa = np.concatenate((compartments_all_ampa, [int(k)] * len(times)))

    if times_all_ampa.tolist() != []:
        indices_all_ampa = np.array(range(len(times_all_ampa)))

        inp_ampa = SpikeGeneratorGroup(len(times_all_ampa), indices_all_ampa, times_all_ampa * second)

        syn_ampa = Synapses(inp_ampa, neuron, on_pre='g_ampa += ' + str(weight_ampa) + '* nS')

        syn_ampa.connect(i=indices_all_ampa.astype(int), j=compartments_all_ampa.astype(int))



    spt_gaba = spike_trains_hierarch(1, ns_gaba_soma, rate_gaba / Hz, t_sim / second, 0., 0., jtr / second)

    times_all_gaba = []
    compartments_all_gaba = []

    for i_s in range(ns_gaba_soma):
        times = spt_gaba[0][i_s]
        condition = times > 0.

        times = np.extract(condition, times)
        times_all_gaba = np.concatenate((times_all_gaba, times))

        compartments_all_gaba = np.concatenate((compartments_all_gaba, [0] * len(times)))

    if times_all_gaba.tolist() != []:
        indices_all_gaba = np.array(range(len(times_all_gaba)))
        inp_gaba = SpikeGeneratorGroup(len(times_all_gaba), indices_all_gaba, times_all_gaba * second)
        syn_gaba = Synapses(inp_gaba, neuron, on_pre='g_gaba += ' + str(weight_gaba) + ' * nS')
        syn_gaba.connect(i=indices_all_gaba.astype(int), j=compartments_all_gaba.astype(int))


    spt_nmda = spike_trains_hierarch(n_cmp, ns_nmda, rate_nmda / Hz, t_sim / second, 0, 0, jtr / second)

    times_all_nmda = []
    compartments_all_nmda = []

    for k in range_of_compartments:
        for i_s in range(ns_nmda):
            times = spt_nmda[k][i_s]
            condition = times > 0.

            times = np.extract(condition, times)
            times_all_nmda = np.concatenate((times_all_nmda, times))

            compartments_all_nmda = np.concatenate((compartments_all_nmda, [int(k)] * len(times)))

    if times_all_nmda.tolist() != []:
        indices_all_nmda = np.array(range(len(times_all_nmda)))
        inp_nmda = SpikeGeneratorGroup(len(times_all_nmda), indices_all_nmda, times_all_nmda * second)
        syn_nmda = Synapses(inp_nmda, neuron, model=eqs_nmda, on_pre=eqs_nmda_when_spike)
        syn_nmda.connect(i=indices_all_nmda.astype(int), j=compartments_all_nmda.astype(int))

# ::: creating folder for data :::
date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

if not os.path.exists(
        '../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']'):
    os.makedirs(
        '../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']')

main_path = os.getcwd()
os.chdir('../DATA/HH/' + str(date_time) + '_[single_run]_[' + morphology + ']')


trace = StateMonitor(neuron, 'v' , record=True)
run(t_sim, report='text')


# ::: plots :::

plot_v_profiles = 'no'

v_pr_amp = 1./5. # 1./7.
t_window = 10 * ms # 15 ms
tp_ds = 1


# diameter

figure(figsize=(12, 10))

plot(lengthT, diameterT,'k')
xlabel('distance from soma [$\mu$m]')
ylabel('dendritic diameter [$\mu$m]')

savefig('dendrite_diameter.png', dpi = 300)
savefig('dendrite_diameter.svg', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[0].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_soma.png', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[100].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_dendrite_middle.png', dpi = 300)


# voltage at soma

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[200].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_dendrite_end.png', dpi = 300)


# voltage at beginning

figure(figsize=(12, 7))

plot(trace.t / ms, trace.v[50].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_dendrite_beginning.png', dpi = 300)





# propagation: heat map

figure(figsize=(12, 7))

contf = contourf(cumsum(neuron.length) / um, trace.t / ms, trace.v.T / mV, cmap='YlOrRd',
                 alpha=1., levels=np.linspace(np.amin(trace.v / mV), np.amax(trace.v / mV), num=100))
colorbar(contf)
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)


# propagation: voltage profiles


if plot_v_profiles == 'yes':

    for t_pl_0_ul in np.arange(0, t_sim/ms-15, t_window/ms):

        t_pl_0 = t_pl_0_ul * ms
        t_pl = t_window

        tp_pl_0 = int(t_pl_0/dt)
        tp_pl = int(t_pl/dt)


        figure(figsize=(10, 15))

        for i in range(tp_pl_0, tp_pl_0 + tp_pl, tp_ds):
            plot(cumsum(neuron.length) / um, dt/ms * i + dt/ms * v_pr_amp * (trace.v[:,i].T / mV - 70.), 'k')

        ylabel('time [ms]', fontsize = 26)
        xlabel('distance from soma [$\mu$m]', fontsize = 26)

        savefig('propagations_voltage_profile_' + str(int(t_pl_0/ms)).rjust(3, '0') + '.png', dpi = 300)



