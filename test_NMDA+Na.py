

import time
import os

from brian2 import *
from inputs import *
import numpy


date_time = time.strftime('%y-%m-%d--%H-%M-%S', time.localtime())

if not os.path.exists('../DATA/HH/' + str(date_time) + '_nmda_synapse'):
	os.makedirs('../DATA/HH/' + str(date_time) + '_nmda_synapse')

main_path = os.getcwd()
os.chdir('../DATA/HH/' + str(date_time) + '_nmda_synapse')




# main parameters of simulation

t_sim = 1000 * ms
dt = 0.05 * ms  # 0.005
method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'


# passive membrane properties

Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm

g_L = 1e-4 * siemens * cm ** -2
E_L = -70 * mV


# voltage-dependent channels

E_Na = 58 * mV
E_K = -80 * mV
g_Na = 12 * msiemens / cm**2 # 12 mS/cm2 : This is the value from (Destexhe,Pare,1999) Some models use 50 ms/cm2 (pyramidal neurons, Wang 1998) or 35 ms/cm2 (interneurons)
							  # Experiments: 1994 - Stuart/Sakmann > 4 mS/cm2
g_K = 7 * msiemens / cm**2  # 7 mS/cm2
v_th = - 63 * mV


# ::: synaptic :::

# reversal potential of synaptic channels

E_ampa = 0 * mV
E_nmda = 0 * mV
E_gaba = -75 * mV


# :: kinetics of synaptic channels ::

# decay constant of ampa & gaba_a

tau_ampa = 5 * ms
tau_gaba = 5 * ms

# nmda : kinetics : with open / closed state eq.

tau_nmda_rise = 2 * ms # ~ time to remove glutamate from cleft
tau_nmda_decay = 100 * ms # ~ time to close channel after opening
alpha_nmda = 0.5 / ms # ~ how much glutamate increases probability of channel opening

# maximal possible conductance of nmda synapse (when all channels are open)
g_nmda_max =  1 * nS

# nmda synapse: Mg2+ block

alpha_v_nmda = 0.062 / mV
beta_v_nmda = 1 / 3.57 # 1/mM
c_Mg = 3 # mM # 1.2


# morphology
morpho = Soma(diameter = 20 * um)
morpho.dendrite = Cylinder(diameter = 2 * um, length = 100 * um, n = 100)


# main hh equation :: Na+ / K+ parameters from Pare 1998

eqs = '''

Im = g_L * (E_L-v) + g_Na * m**3 * h * (E_Na-v) + g_K * n**4 * (E_K-v) : amp/meter**2
	
dm/dt = alpham * (1-m) - betam * m : 1
dh/dt = alphah * (1-h) - betah * h : 1
dn/dt = alphan * (1-n) - betan * n : 1

alpham = - 0.32/mV * (v - v_th - 13 * mV) / (exp(-(v - v_th - 13 * mV)/(4 * mV)) - 1) /ms : Hz
betam = 0.28/mV * (v - v_th - 40 * mV) / (exp((v - v_th - 40 * mV)/(5 * mV)) -1) /ms : Hz
	
alphah = 0.128 * exp(-(v - v_th - 17 * mV) / (18 * mV)) /ms : Hz
betah = 4 / (1 + exp(-(v - v_th - 40 * mV) / (5 * mV))) /ms : Hz
	
alphan = -0.032 / mV * (v - v_th - 15 * mV) / (exp(-(v - v_th - 15 * mV) / (5 * mV)) - 1) / ms : Hz
betan = 0.5 * exp(-(v - v_th - 10 * mV) / (40 * mV)) /ms : Hz

Is = g_ampa * (E_ampa - v) + g_gaba * (E_gaba - v) + g_nmda * (E_nmda - v) / (1 + beta_v_nmda * exp(-alpha_v_nmda * v) * c_Mg) + I : amp (point current)

dg_ampa/dt = -g_ampa/tau_ampa : siemens
dg_gaba/dt = -g_gaba/tau_gaba : siemens

I : amp
g_Na : siemens / meter**2
g_K : siemens / meter**2	
g_nmda : siemens

'''


# nmda synapse eq.

eqs_nmda = '''

ds/dt = - (1/tau_nmda_decay)*s + alpha_nmda*x*(1-s) : 1
g_nmda_post = g_nmda_max * s : siemens (summed)
dx/dt = - (1/tau_nmda_rise) * x : 1

'''


neuron = SpatialNeuron(morphology=morpho, model=eqs, method=method, dt=dt, Cm=Cm, Ri=Ri)

neuron.v = -70 * mV
neuron.h = 0.
neuron.m = 0.
neuron.n = 0.
neuron.I = 0.

neuron.g_Na[:] = g_Na
neuron.g_K[:] = g_K


# synaptic stimulation

indices = array([0, 1, 2, 3])
times = array([250.,260.,750.,760.]) * ms  # 5 ms
compartments = array([25, 25, 75, 75])


input = SpikeGeneratorGroup(len(times), indices, times)
S = Synapses(input, neuron, eqs_nmda, on_pre='x+=1', dt = 0.05 * ms)
S.connect(i=indices.astype(int), j=compartments.astype(int))

S.s = 0.


# recorded variables

trace_v = StateMonitor(neuron, 'v' , record=True)
trace_s = StateMonitor(S, 's' , record=True)
trace_g = StateMonitor(neuron, 'g_nmda' , record=True)


# run

run(t_sim/2, report='text')

neuron.I[75] = 20 * pA

run(t_sim/2, report='text')

neuron.I[75] = 0 * pA



# PLOTTING VOLTAGE

figure(figsize=(12, 7))

plot(trace_v.t / ms, trace_v.v[0].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_soma.png', dpi = 300)
show()



# PLOTTING SYNAPTIC CURRENTS

figure(figsize=(12, 7))

plot(trace_s.s[0], color = 'k')
xlabel('time [ms]')
ylabel('state synaptic gate (0-1)')

savefig('v_soma.png', dpi = 300)
show()


figure(figsize=(12, 7))

plot(trace_s.s[1], color = 'k')
xlabel('time [ms]')
ylabel('state synaptic gate (0-1)')

savefig('v_soma.png', dpi = 300)
show()



# propagation: heat map

figure(figsize=(12, 7))

contf = contourf(cumsum(neuron.length) / um, trace_v.t / ms, trace_v.v.T / mV,
                 alpha=1., levels=np.linspace(np.amin(trace_v.v / mV), np.amax(trace_v.v / mV), num=100))
colorbar(contf)
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)
show()



figure(figsize=(12, 7))

contf = contourf(trace_g.g_nmda.T / nS)
colorbar(contf)
title('NMDA conductance of compartments')
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)
show()









