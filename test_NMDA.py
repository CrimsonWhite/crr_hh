


from brian2 import *
from inputs import *
import numpy



t_sim = 1000 * ms
dt = 0.05 * ms  # 0.005

method = 'rk2'  # 'linear', 'exponential_euler', 'rk2 or 'heun'





Cm = 1e-6 * farad * cm ** -2
Ri = 100 * ohm * cm

g_L = 1e-4 * siemens * cm ** -2
E_L = -70 * mV


G_NMDA =  1 * nS


alpha_v_nmda = 0.062 / mV
beta_v_nmda = 1 / 3.57 # 1/mM
c_Mg = 1.2 # mM



morpho = Soma(diameter = 20 * um)
morpho.dendrite = Cylinder(diameter = 1 * um, length = 100 * um, n = 100)



eqs = '''


Im = g_L * (E_L - v) : amp/meter**2
Is = G_NMDA * g * (0 * mV - v) : amp (point current)
g : 1

'''


eqs = '''


Im = g_L * (E_L - v) + I : amp/meter**2
Is = G_NMDA * g * (0 * mV - v) / (1 + beta_v_nmda * exp(-alpha_v_nmda * v) * c_Mg) : amp (point current)
g : 1
I : amp/meter**2

'''












tau_NMDA_rise = 2 * ms # decay rate
tau_NMDA_decay = 100 * ms
alpha = 0.5 / ms # decay rate

eqs_syn = '''

ds/dt = - (1/tau_NMDA_decay)*s + alpha*x*(1-s) : 1
g_post = s : 1 (summed)
dx/dt = - (1/tau_NMDA_rise) * x : 1

'''


neuron = SpatialNeuron(morphology=morpho, model=eqs, method=method, dt=dt, Cm=Cm, Ri=Ri)

neuron.v = -70 * mV



indices = array([0, 1, 2, 3])
times = array([250.,260.,750.,760.]) * ms  # 5 ms
compartments = array([25, 25, 75, 75])


input = SpikeGeneratorGroup(len(times), indices, times)
S = Synapses(input, neuron, eqs_syn, on_pre='x+=1', dt = 0.05 * ms)
S.connect(i=indices.astype(int), j=compartments.astype(int))

S.s = 0.


trace_v = StateMonitor(neuron, 'v' , record=True)
trace_s = StateMonitor(S, 's' , record=True)
trace_g = StateMonitor(neuron, 'g' , record=True)

run(t_sim/2, report='text')

neuron.I[75] = 1200 * uA/cm**2

run(t_sim/2, report='text')





# PLOTTING VOLTAGE

figure(figsize=(12, 7))

plot(trace_v.t / ms, trace_v.v[0].T / mV, color = 'k')
xlabel('time [ms]')
ylabel('membrane potential [mV]')

savefig('v_soma.png', dpi = 300)
show()



# PLOTTING SYNAPTIC CURRENTS

figure(figsize=(12, 7))

plot(trace_s.s[0], color = 'k')
xlabel('time [ms]')
ylabel('state synaptic gate (0-1)')

savefig('v_soma.png', dpi = 300)
show()


figure(figsize=(12, 7))

plot(trace_s.s[1], color = 'k')
xlabel('time [ms]')
ylabel('state synaptic gate (0-1)')

savefig('v_soma.png', dpi = 300)
show()



# propagation: heat map

figure(figsize=(12, 7))

contf = contourf(cumsum(neuron.length) / um, trace_v.t / ms, trace_v.v.T / mV,
                 alpha=1., levels=np.linspace(np.amin(trace_v.v / mV), np.amax(trace_v.v / mV), num=100))
colorbar(contf)
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)

show()



figure(figsize=(12, 7))

contf = contourf(trace_g.g.T)
colorbar(contf)
title('NMDA conductance of compartments')
xlabel('distance from soma [$\mu$m]')
ylabel('time [ms]')

savefig('propagations_color_map.png', dpi = 300)

show()









